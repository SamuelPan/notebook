```properties
spring.application.name=fundcoref
server.port=8080
server.servlet.context-path=/fundcore
#fundCore.isNewStruct=false
# 队列声明
#fundcore.mq.cebtrust=false
fundcore.mq.aibank=true
# vault配置
caijiajia.vault.url=http://vault.ali-bj-sit01.shuheo.net:80
caijiajia.vault.roleId=${spring.application.name}
caijiajia.vault.secretId=bef1e131-ffd9-25d2-b828-f4a879c7d6fc
# dev:d7067f63-5656-8802-9226-775406936994
#caijiajia.aliyun.vault.paths.bigdata.path=alicloud/creds/fundcore-bigdata
# 动态数据源-vault
caijiajia.vault.pros.fundcore.path=database/creds/${spring.application.name}_cjjloan
caijiajia.vault.pros.yuecai.path=database/creds/${spring.application.name}_cjjloan
caijiajia.vault.pros.cebtrust.path=database/creds/${spring.application.name}_cjjloan
#若是需要有选择的初始化数据源，可加上如下配置，不配置则所有动态数据源都初始化
caijiajia.db.routing.initKeys=JDGOLDBAR
# 动态数据源配置
fundcore.data.url=cjjloan.db.ali-bj-sit01.shuheo.net
fundcore.data2.url=cjjloan.db.ali-bj-sit01.shuheo.net
caijiajia.db.routing.beanName=dataSource
caijiajia.db.routing.policyBeanName=fundcoreRoutingPolicy
# 数据库实例
fundcore.yuecai.instance=fundcore
fundcore.citiccfc.instance=citiccfc
fundcore.cebtrust.instance=cebtrust
fundcore.xwyhfund.instance=xwyhfund
fundcore.fumingfund.instance=fumingfund
fundcore.zyxjfund.instance=zyxjfund
fundcore.aibank.instance=aibankfund
fundcore.bobank.instance=bobankfund
fundcore.mibank.instance=mibankfund
fundcore.qhjt.instance=qihufund
fundcore.fuqiceb.instance=fuqicebfund
fundcore.fuqihxbfund.instance=fuqicebfund
fundcore.fuqichbhfund.instance=fuqichbhfund
fundcore.tcqzcfund.instance=tcqzcfund
fundcore.sdictktrustfund.instance=sdictktrustfund
fundcore.sdictktrustfund.username=sdictktrustfund
fundcore.bilibaofund.instance=bilibaofund
fundcore.sdictktrustfund.password=1qaz@WSX
fundcore.yillionfund.instance=yillionfund
fundcore.aibankdcpfund.instance=aibankdcpfund
fundcore.srcbank.instance=srcbfund
fundcore.fenqilefund.instance=fenqilefund
fundcore.jdgoldbar.instance=capitalaccount
fundcore.wlskfund.instance=wlskfund
fundcore.wlskfund.username=tomcat
fundcore.wlskfund.password=1qaz@WSX


caijiajia.db.databases[35].dataSource.model=DEFAULT
caijiajia.db.databases[35].dataSource.username=test
caijiajia.db.databases[35].dataSource.password=test

# 慢sql打印contract
caijiajia.db.routing.log.slowSqlThreshold=10000
# 分布式锁
redis.distributedlock.configByRedisplus=true
#decision
url.decision=http://decision.apps.sit.caijj.net/decision
# redis配置-决策流使用
redis.clients.bizflow.hostName=fundcore.redis.ali-bj-sit01.shuheo.net
redis.clients.bizflow.port=6379
redis.clients.bizflow.database=10
redis.clients.bizflow.usePool=false
redis.clients.bizflow.maxConnTotal=100
redis.clients.bizflow.maxConnIdle=10
redis.clients.bizflow.model=NORMAL
# redis配置-token使用
redis.clients.fundcore.hostName=fundcore.redis.ali-bj-sit01.shuheo.net
redis.clients.fundcore.port=6379
redis.clients.fundcore.database=4
# redis配置-jasmine使用
redis.clients.jasmineRedisAutoConfClient.hostName=fundcore.redis.ali-bj-sit01.shuheo.net
redis.clients.jasmineRedisAutoConfClient.port=6379
redis.clients.jasmineRedisAutoConfClient.database=4
# redis配置-sequence关联
#redis.clients.sequenceRedisAutoConfClient.hostName=arch01.grr6uh.0001.cnw1.cache.amazonaws.com.cn
redis.clients.sequenceRedisAutoConfClient.hostName=fundcore.redis.ali-bj-sit01.shuheo.net
redis.clients.sequenceRedisAutoConfClient.port=6379
redis.clients.sequenceRedisAutoConfClient.database=7
# rabbitmq配置

caijiajia.rabbitmq.address=rabbitmq01.ali-bj-dev01.shuheo.net:5672
caijiajia.rabbitmq.password=admin
caijiajia.rabbitmq.username=admin
caijiajia.splitcluster.rabbitmq.address=rabbitmq01.ali-bj-dev01.shuheo.net:5672
caijiajia.splitcluster.rabbitmq.password=admin
caijiajia.splitcluster.rabbitmq.username=admin

caijiajia.amqp.addresses=rabbitmq01.ali-bj-dev01.shuheo.net
caijiajia.amqp.mandatory.enableProducer=true
caijiajia.amqp.password=admin
caijiajia.amqp.username=admin
caijiajia.amqp.virtualHost=/

#caijiajia.amqp01.addresses=amqp-cn-4591j61c6009.mq-amqp.cn-beijing-327844-a-internal.aliyuncs.com
#caijiajia.amqp01.instanceId=amqp-cn-4591j61c6009
#caijiajia.amqp01.username=LTAI5tBLGqta4Nrx6EFnkpEq
#caijiajia.amqp01.password=vMPBFGg1SqSSVxprvHP24t728r2X7k
#caijiajia.amqp01.virtualHost=sit

caijiajia.amqp01.addresses=rabbitmq01.ali-bj-dev01.shuheo.net:5672
caijiajia.amqp01.instanceId=1017769406130645
caijiajia.amqp01.password=admin
caijiajia.amqp01.username=admin
caijiajia.amqp01.virtualHost=/

#rabbitmq.address=rabbitmq01.ali-bj-dev01.shuheo.net:15672
#rabbitmq.password=admin
#rabbitmq.username=admin
#url.rabbitplus=http://rabbitplus.apps.sit.caijj.net/rabbitplus
# 启动阿里云，设置为aws或不配置则启动亚马逊云
cn.caijiajia.oss.cloud=switchByConsole
caijiajia.aliyun.accessKeyID=AKIAPBJNOWIREV6FM7TQ
caijiajia.aliyun.accessKeySecret=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
#默认为external，本地访问需在application-local.properties配置external使用外网访问。
#caijiajia.aliyun.oss.accessType=external
# s3桶名配置
aws.s3.bucketName.bankgateway=bankgateway-sit
aws.s3.bucketName.contract=lattebank-contract-sit
aws.s3.bucketName.filegw=lattebank-filegw-sit
aws.s3.bucketName.secretkeys=secretkeys
#caijiajia.aws.accessKey=AKIAPBJNOWIREV6FM7TQ
#caijiajia.aws.secretKey=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
#caijiajia.aws.s3.region=CN_NORTH_1
zooKeeper.connectString=172.0.10.203:2181,172.0.10.203:2182,172.0.10.203:2183
bankgateway.secretKeyPath=bankgatewaySecret
# 测试环境
yuecai.appid=T2666173391470390
#测试环境
yuecai.assetid=XT201905160006584
#生产环境
#yue.pro.assetid=XT201904280002786
# 生产环境
#yuecai.appid=P2667354946341213
url.standingbook=http://standingbook.apps.sit.caijj.net/standingbook
caijiajia.configservice.host=configservice.apps01.ali-bj-sit01.shuheo.net
# consul配置-本地未注册到consul，需要配置该项
caijiajia.invoke.imagedata.url=http://imagedata.apps01.ali-bj-sit01.shuheo.net/imagedata
caijiajia.invoke.guardplus.url=http://guardplus.apps01.ali-bj-sit01.shuheo.net/guardplus
caijiajia.invoke.magicconf.url=http://magicconf.apps01.ali-bj-sit01.shuheo.net/magicconf
caijiajia.invoke.confplus.url=http://confplus.apps01.ali-bj-sit01.shuheo.net/confplus/
caijiajia.invoke.sequence.url=http://sequence.apps01.ali-bj-sit01.shuheo.net/sequence
caijiajia.invoke.personas.url=http://personas.apps01.ali-bj-sit01.shuheo.net/personas
caijiajia.invoke.obtaindata.url=http://obtaindata.apps01.ali-bj-sit01.shuheo.net
caijiajia.invoke.contract.url=http://contract.apps01.ali-bj-sit01.shuheo.net/contract
caijiajia.invoke.card.url=http://card.apps01.ali-bj-sit01.shuheo.net
caijiajia.invoke.divisioncode.url=http://divisioncode.apps01.ali-bj-sit01.shuheo.net
caijiajia.invoke.coupon.url=http://coupon.apps01.ali-bj-sit01.shuheo.net
caijiajia.invoke.loan.url=http://loan.apps.apps01.ali-bj-sit01.shuheo.net
caijiajia.invoke.magichistoryapi.url=http://magichistoryapi.apps01.ali-bj-sit01.shuheo.net
caijiajia.invoke.magicrealtimeapi.url=http://magicrealtimeapi.apps01.ali-bj-sit01.shuheo.net
caijiajia.invoke.deviceinfo.url=http://deviceinfo.apps01.ali-bj-sit01.shuheo.net
caijiajia.invoke.raisecredit.url=http://raisecredit.apps01.ali-bj-sit01.shuheo.net
caijiajia.invoke.efuel.url=http://efuel.apps.apps01.ali-bj-sit01.shuheo.net
caijiajia.invoke.decisionflow.url=http://decisionflow.apps01.ali-bj-sit01.shuheo.net
caijiajia.invoke.payment.url=http://payment.apps01.ali-bj-sit01.shuheo.net
#caijiajia.invoke.fundgw.url=http://localhost:8088/fundgw
caijiajia.invoke.fundgw.url=http://fundgw.apps01.ali-bj-sit01.shuheo.net
#caijiajia.invoke.fundgw.url=http://localhost:8088/fundgw
caijiajia.invoke.filegw.url=http://filegw.apps01.ali-bj-sit01.shuheo.net
caijiajia.invoke.egressgateway.url=http://egressgateway.apps01.ali-bj-sit01.shuheo.netcaijiajia.invoke.filegw.url=http://localhost:8089/filegw
caijiajia.invoke.esign.url=http://esign.apps01.ali-bj-sit01.shuheo.net
caijiajia.invoke.redisplus.url=http://redisplus.apps01.ali-bj-sit01.shuheo.net
caijiajia.invoke.proplus.url=http://proplus.apps01.ali-bj-sit01.shuheo.net
caijiajia.invoke.bizgwconfig.url=http://bizgwconfig.apps01.ali-bj-sit01.shuheo.net
caijiajia.invoke.creditdata.url=http://creditdata.apps01.ali-bj-sit01.shuheo.net
caijiajia.invoke.configservice.url=http://configservice.apps01.ali-bj-sit01.shuheo.net
caijiajia.invoke.mockservice.url=http://mockservice.apps01.ali-bj-sit01.shuheo.net
caijiajia.invoke.ossplus.url=http://ossplus.apps01.ali-bj-sit01.shuheo.net
gw.appId=fundcore
gw.receiveSystem=fundcore
gw.requestMsgId=zssssDh
gw.sendSystem=fundcore
gw.msgDirect=0
gw.version=1.0

applyNoFormat=yccr%s%013d
url.payment=http://payment.apps.sit.caijj.net/payment
fundcore.datapurge.config.name=fundcore_datapurge_config
spring.devtools.restart.trigger-file=trigger
```

