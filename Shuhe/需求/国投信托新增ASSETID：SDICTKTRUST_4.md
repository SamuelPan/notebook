REQ地址：http://jira.caijj.net/browse/REQ-12685



#### 发布配置：



- [ ] **配置checklist：**[国投信托新增ASSETID：SDICTKTRUST_4生产配置项checlist - 数禾中台（待归档） - 数禾知识库 (caijj.net)](http://wiki.caijj.net/pages/viewpage.action?pageId=152015678)



- [ ] **funcore confplus配置：**

  **esign配置：**

  fundcore

  confplus key：fundcore_bank_esign_mapping

  添加配置：

  ```json
  "SDICTKTRUST.SDICTKTRUST_4.LEND": {
      "bank": "SDICTKTRUST",
      "interfaceId": [
        "sdictktrust.dkht.contract_1"
      ]
    },
    "SDICTKTRUST.SDICTKTRUST_4.CREDIT": {
      "bank": "SDICTKTRUST",
      "interfaceId": [
        "sdictktrust.grxxcjsqs.contract_1",
        "sdictktrust.grzxsqs.contract_1",
        "sdictktrust.jkfwxy.contract",
        "sdictktrust.bhgrzxsqs.contract"
      ]
    },
    "SDICTKTRUST.SDICTKTRUST_4": {
      "bank": "SDICTKTRUST",
      "interfaceId": [
        "sdictktrust.dkht.contract_1"
      ]
    },
  ```

  **信托账户余额查询配置：**

  **资金平台组：**

  - [ ] 1、信托余额查询：

    assetid_trust_plan：信托余额查询页面下拉框值

    ```json
    fundcore
    confplus key：assetid_trust_plan
    
    新增配置：
    {
          "SDICTKTRUST_4": "国投泰康黄雀127-3"
     }
    ```

    fundcore_assetid_data ：assetid与信托号对应关系

    ```json
    **资产名称配置**
    fundcore
    confplus key：fundcore_assetid_data
    新增配置:
    "SDICTKTRUST_4": "10050048"
    
    // "SDICTKTRUST_5": "DyccbCommonReceiveResponseHandler","SDICTKTRUST_6": "10050053",
    ```

  - [ ] 2、每日放款量
    
    ```json
    ## fundcore
    fundcore_bank_assetid
    bank与assetid对应关系
    ```

- [ ] **文件网关配置：**

  #### 报文网关参数配置：

  | 组名称(groupName) | 键名称(keyName) | 值(val)  | 所属应用(innerAppName) |
  | :---------------- | :-------------- | :------- | :--------------------- |
  | SDICTKTRUST_4     | providerId      | 10050021 | fundcored              |
  | SDICTKTRUST_4     | fundsId         | 10050021 | fundcored              |

  - [ ] adb插数据
  
    ```sql
    INSERT INTO  dw.`d_fa_fnd_bank_assetid_info` (`fund_code`, `fund_name`, `asset_id`, `asset_name`, `asset_attribute`, `fund_attribute`, `switch`, `repay_advance_hold_flag`)
    values
    ('SDICTKTRUST', '国投泰康信托', 'SDICTKTRUST_7', '国投泰康黄雀127-6', '重资产', '信托类', 'Y', 'N');
    ```
  
    
