**接口配置：**

| 接口ID(interfaceId) | 接口名称(interfaceName) | 版本(version) | 脚本表ID(messageProcessId) | 是否可用(enable) | 扩展属性(properties) | 内部系统(innerSystem) | 所属应用(innerAppName) |
| :------------------ | :---------------------- | :------------ | :------------------------- | :--------------- | :------------------- | :-------------------- | :--------------------- |
| psbcxj.repay.reply  | 还款结果反向通知接口    | 1.0.0         | psbcxj.repay.reply.process | Y                |                      | fundcoree             | fundgw                 |



**app接口映射：**

| appID(appId)    | 接口ID(interfaceId) | 网关系统地址(fundgwId) | 超时时间(timeout) | 通信表ID(communicationId)        | 业务属性(bizProperties) | 扩展属性(extProperties) | 状态(enable) | 所属应用(innerAppName) |
| :-------------- | :------------------ | :--------------------- | :---------------- | :------------------------------- | :---------------------- | :---------------------- | :----------- | :--------------------- |
| fundcore.psbcxj | psbcxj.repay.reply  | fundgw                 | 5000              | psbcxj.repay.reply.communication |                         |                         | Y            | fundcored              |



**通信配置：**

| ID(communicationId)              | 协议类型(protocol) | 接口ID(interfaceId) | 连接类型(connectionType) | 发送报文格式(sendMessageFormat) | 接收报文格式(recvMessageFormat) | URL(uri)           | 连接超时(connectTimeout) | 读超时(readTimeout) | 扩展属性(properties)                                         | 所属应用(innerAppName) |
| :------------------------------- | :----------------- | :------------------ | :----------------------- | :------------------------------ | :------------------------------ | :----------------- | :----------------------- | :------------------ | :----------------------------------------------------------- | :--------------------- |
| psbcxj.repay.reply.communication | HTTPS              | psbcxj.repay.reply  | SERVER                   | TEXT                            | TEXT                            | psbcxj/repay/reply | 5000                     | 5000                | HTTP_TYPE=POST;;MOCK_URL=http://egressgateway.apps01.ali-bj-sit01.shuheo.net/egress_PSBCXJ/facadeuatpsbcxj/repay/reply | fundgw                 |



**报文处理：**

| 脚本ID(messageProcessId)   | 组装脚本名称(assembleRequestClassName) | 组装脚本(assembleRequestTemplate)                            | 解析脚本名称(parseResponseClassName) | 解析脚本(parserResponseTemplate)                             | 是否基类(isBaseClass) | 所属应用(innerAppName) | 操作 |
| :------------------------- | :------------------------------------- | :----------------------------------------------------------- | :----------------------------------- | :----------------------------------------------------------- | :-------------------- | :--------------------- | :--- |
| psbcxj.repay.reply.process | RepayReplyReceiveReq                   | [RepayReplyReceiveReq](../groovy/RepayReplyReceiveReq.groovy) | RepayReplyReceiveResp                | [RepayReplyReceiveResp](../groovy/RepayReplyReceiveResp.groovy) | N                     | fundgw                 |      |

