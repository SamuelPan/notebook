```sql
delete from bf_process_info
where biz_serial in ('BT202108301065567_sdictktrust_lend_success') and order_number>1;

update bf_flow_info f inner join 
(select biz_serial,node from bf_process_info where biz_serial in ('BT202108301065567_sdictktrust_lend_success') and order_number=1) p
 on p.biz_serial = f.biz_serial set f.node = REPLACE(p.node,"true","false");

UPDATE bf_process_info set exec_status = 'PAUSED'  where  biz_serial in ('BT202108301065567_sdictktrust_lend_success') and order_number = 1;
 
UPDATE bf_flow_info set exec_status = 'PAUSED'  where  biz_serial in ('BT202108301065567_sdictktrust_lend_success');
```

