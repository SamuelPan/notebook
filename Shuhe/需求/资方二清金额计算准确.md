[[CD2-1019\] 资方二清金额计算准确 - 上海数禾项目管理系统 (caijj.net)](http://jira.caijj.net/browse/CD2-1019)

[[REQ-12356\] 资方二清金额计算准确 - 上海数禾项目管理系统 (caijj.net)](http://jira.caijj.net/browse/REQ-12356)



```json
{
	"...": "...",
	"repaymentCreditSectionSettleToAsset": "0",
	"surchargeCondition": "1",
	"triggerTwiceSettleToAsset": "0",
	"fundCost": 10.99, // 资金方成本
	"settlementRulesConfig": { // 算费规则配置
		"repayPlanConfig": { // 还款计划配置
			"repaymentDateRule": "REPAYMENT_DATE_RULE1", // REPAYMENT_DATE_RULE1(放款日为1～28日...),REPAYMENT_DATE_RULE2(放款日为1～31日...)
			"principalInterestCalculationRules": "EQUAL_LOAN_PAYMENT_STANDARD" // EQUAL_LOAN_PAYMENT_STANDARD(等额本息-标准PMT), EQUAL_LOAN_PAYMENT_BY_DAY(等额本息-按日计息PMT), EQUAL_PRINCIPAL_PAYMENT(等本等息)
		},
		"settleEarlyCalfeeConfig": { // 提前结清算费配置
			"settleEarlyCalculationFormula": "REMAIN_PRINCIPAL_INTEREST_DAY_TO_DAY", // REMAIN_PRINCIPAL_INTEREST_DAY_TO_DAY(剩余本息按日计息), REMAIN_PLANE_REPAY_ALL(剩余计划全额还完)
			"settleEarlyCalculateInterestDays": "DAY_OF_EARLY_REPAYMENT", // DAY_OF_EARLY_REPAYMENT(提前还款当日),FIXED_FOR_30_DAYS(固定30天)
			"plusDays": 0, // 上一个应还款日需加天数
			"settleEarlyChargeHandlingFee": false, // 是否收取提前结清手续费：true-是，false-否
			"settleEarlyHandlingFeeProportion": 0.00 // 提前结清手续费比例
		},
		"overdueRepayCalfeeConfig": { // 逾期还款算费配置
			"overdueRepayCalculationFormula": "OVERDUE_CALCULATE_INTEREST_BY_DAY", // OVERDUE_CALCULATE_INTEREST_BY_DAY("逾期按日计息")
			"overdueGracePeriod": 0, // 逾期宽限期
			"chargePenaltyInterest": false, // 是否收取罚息，true-是，false-否
			"penaltyInterestAnnualInterestRate": "AS_FUND_COST", // AS_FUND_COST(同资金成本),FUND_COST_FLOAT_UP(资金成本上浮)
			"floatUpProportion": 0.00, // 资金成本上浮
			"penaltyInterestChargeBase": "OVERDUE_PRINCIPAL", // OVERDUE_PRINCIPAL(逾期本金收罚息),OVERDUE_INTEREST(逾期利息收罚息)
			"penaltyInterestCalculateDays": 0 // 罚息计息天数
		},
		"settlementConfig": { // 结算配置
			"supportOverdueCompensation": false, // 是否支持逾期代偿：true-是，false-否
			"supportOverdueRepurchase": false // 是否支持逾期回购：true-是，false-否
		},
		"interestAmountCalculateAccuracy": { // 利息&金额计算精度
			"interestDecimalPlaces": 0, // 利息计算保留小数位
			"interestDecimalPlacesRule": "ROUNDING", // ROUNDING(四舍五入)，CEILING（全部进位），FLOOR（全部舍去）
			"amountDecimalPlaces": 0, // 金额计算保留小数位
			"amountDecimalPlacesRule": "ROUNDING"
		}
	}
}
```

