package groovy.template.dyccb

import cn.caijiajia.fundgwserver.dto.MessageCommonContext
import cn.caijiajia.fundgwserver.handler.AbstractReceiveResponseHandler
import cn.caijiajia.fundgwserver.util.ContentTypes
import com.alibaba.fastjson.JSON
import com.alibaba.fastjson.JSONObject
import groovy.util.logging.Slf4j
import org.apache.commons.collections.MapUtils
import org.apache.commons.lang3.time.DateFormatUtils
import org.springframework.web.context.request.RequestContextHolder
import org.springframework.web.context.request.ServletRequestAttributes

import javax.crypto.Cipher
import javax.crypto.KeyGenerator
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec
import javax.servlet.http.HttpServletResponse
import java.security.MessageDigest

/**
 *
 * 东营银行通用反向调用响应处理
 *
 * @author liuchenglong @date 2021/08/12 上午11:11
 */
@Slf4j
abstract class DyccbCommonReceiveResponseHandler extends AbstractReceiveResponseHandler {

    protected static final def GROUP_NAME = "fundcore.dyccb.group"
    protected static final def TIME_PATTERN = "YYYYMMddHHmmssSSS"
    private static final String CHARSET = "UTF-8"
    protected static final int SUCCESS_CODE = 200

    /**
     * 算法/模式/填充模式
     */
    protected static final String ALGORITHM_PATTERN_FILL_PATTERN = "AES/CBC/PKCS5Padding"
    protected ThreadLocal<String> randomKey = new ThreadLocal<>()
    protected ThreadLocal<String> data = new ThreadLocal<>()


    @Override
    String buildResponse(String result, MessageCommonContext context) throws Exception {
        randomKey.set(getKey())
        def interfaceId = context.getMessageTemplateDto().getInterfacePo().getInterfaceId()
        log.info("东营银行反向调用响应, interfaceId:{},result:{},接口请求randomKey为:{}", interfaceId, result, randomKey.get())
        def requestData = [:]
        JSONObject resultObject = JSON.parseObject(result)
        requestData.head = buildHead(resultObject)
        requestData.requestBody = buildBody(resultObject)
        def aesBody = encryptByAES(JSON.toJSONString(requestData), randomKey.get())
        data.set(JSON.toJSONString(requestData))
        // 添加响应头
        addResponseHeader(buildResponseHeader(context))
        return aesBody
    }

    Map<String, String> buildResponseHeader(MessageCommonContext context) {
        def appId = context.getMessageTemplateDto().getTempGwAppPo().getAppId()
        def interfaceId = context.getMessageTemplateDto().getInterfacePo().getInterfaceId()
        def param = [:] as HashMap
        param["X-Uop-Api-Code"] = gateWayThirdPartAttributeService.getGroupValues(appId, interfaceId)
        param["X-Uop-App-Id"] = gateWayThirdPartAttributeService.getGroupValues(GROUP_NAME, "appId")
        param["X-Uop-Unique-No"] = UUID.randomUUID().toString().replace("-", "").toUpperCase()
        param["X-Uop-Timestamp"] = DateFormatUtils.format(System.currentTimeMillis(), TIME_PATTERN)
        def signTxt = param.clone()
        log.info("东营银行反向调用响应,buildResponseHeader，interfaceId:{}, 接口响应体明文:{}, randomKey:{}",
                interfaceId, data.get(), randomKey.get())
        signTxt["Body"] = SHA256(data.get())
        param["X-Uop-Signature"] = encryptionService.sign(context, compareToStr(signTxt))
        param["X-Uop-Encrypted-Key"] = encryptionService.encryption(context, randomKey.get())
        param.put(ContentTypes.CONTENT_KEY, ContentTypes.JSON_UTF_8)
        log.info("东营银行反向调用响应,buildResponseHeader，interfaceId:{}, 响应头:{}", interfaceId, JSON.toJSONString(param))
        randomKey.remove()
        data.remove()
        return param
    }


    /**
     * 设置响应头
     *
     * @param headerMap 响应头map
     */
    static void addResponseHeader(Map<String, String> headerMap) {
        if (MapUtils.isEmpty(headerMap)) {
            log.info("东营银行反向调用响应，无响应头需要设置")
            return
        }
        log.info("东营银行反向调用响应，需要设置的响应头有:{}", JSON.toJSONString(headerMap))
        HttpServletResponse response = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getResponse()
        headerMap.each { key, value ->
            response.setHeader(key, value)
        }
    }

    /***
     * 获取128位AES随机密钥
     *
     * @return 128位AES随机密钥
     */
    static String getKey() {
        KeyGenerator kgen = KeyGenerator.getInstance("AES")
        kgen.init(128)
        byte[] encoded = kgen.generateKey().getEncoded()
        return Base64.getEncoder().encodeToString(encoded)
    }

    /**
     * AES 加密
     *
     * @param data 待加密数据
     * @param key 加密时使用的盐
     * @return
     */
    static String encryptByAES(String content, String aesKey) {
        try {
            Cipher cipher = Cipher.getInstance(ALGORITHM_PATTERN_FILL_PATTERN)
            IvParameterSpec iv = new IvParameterSpec(initIv(ALGORITHM_PATTERN_FILL_PATTERN))
            cipher.init(1, new SecretKeySpec(Base64.getDecoder().decode(aesKey.getBytes()), "AES"), iv)
            byte[] encryptBytes = cipher.doFinal(content.getBytes(CHARSET))
            return Base64.getEncoder().encodeToString(encryptBytes)
        } catch (Exception e) {
            log.warn("AES加密异常", e)
        }
        return null
    }

    /***
     * 初始化IV
     *
     * @param fullAlg
     * @return
     */
    private static byte[] initIv(String fullAlg) {
        byte[] iv
        int i
        try {
            Cipher cipher = Cipher.getInstance(fullAlg)
            int blockSize = cipher.getBlockSize()
            iv = new byte[blockSize]

            for (i = 0; i < blockSize; ++i) {
                iv[i] = 0
            }

            return iv
        } catch (Exception var5) {
            log.warn("initIv exist exception:{}", var5)
            int blockSize = 16
            iv = new byte[blockSize]

            for (i = 0; i < blockSize; ++i) {
                iv[i] = 0
            }

            return iv
        }
    }

    static String SHA256(final String strText) {
        try {
            MessageDigest messageDigestIn = MessageDigest.getInstance("SHA-256")
            messageDigestIn.update(strText.getBytes(CHARSET))
            byte[] str = messageDigestIn.digest()
            if (str != null) {
                return Base64.getEncoder().encodeToString(str)
            }
        } catch (Exception e) {
            log.warn("SHA-256异常", e)
            return null
        }
        return null
    }

    /***
     * 排序获取Str
     * @param signMap
     * @return
     */
    static String compareToStr(def signMap) {
        StringBuilder content = new StringBuilder()
        List<String> keys = new ArrayList(signMap.keySet())
        Collections.sort(keys)
        for (int i = 0; i < keys.size(); ++i) {
            String key = (String) keys.get(i)
            String value = (String) signMap.get(key)
            content.append(i == 0 ? "" : "&").append(key).append("=").append(value)
        }
        return content.toString()
    }

    static def buildHead(JSONObject param) {
        log.info("东营银行反向调用响应，buildHead, 接收内部系统的callbackData:{}", param)
        JSONObject callbackData = param.callbackData as JSONObject
        def head = [:]
        head.merchantSerialNo = callbackData.applyNo
        head.idemSerialNo = callbackData.seqNo
        head.serialNo = callbackData.seqNo
        def now = DateFormatUtils.format(System.currentTimeMillis(), "yyyy-MM-dd HH:mm:ss")
        head.returnDate = now
        head.success = "true"
        head.respMsg = "执行成功"
        head.responseType = "SUCCESS"
        head.sysTime = now
        head.respCode = "000000"
        log.info("东营银行反向调用响应，buildHead组装完成,组装结果head:{}", head)
        return head
    }

    /**
     * 方法请求参数
     *
     * @return 构建完成的请求参数
     */
    abstract def buildBody(JSONObject param)
}
