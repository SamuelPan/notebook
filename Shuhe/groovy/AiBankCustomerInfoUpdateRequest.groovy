package cn.caijiajia.fundgwserver.groovy.aibank

import cn.caijiajia.fundgwcommon.base.GateWayMessageRequest
import cn.caijiajia.fundgwcommon.code.CommonCodes
import cn.caijiajia.fundgwserver.dto.MessageCommonContext
import cn.caijiajia.fundgwserver.handler.biz.AibankCommonClientRequest
import cn.caijiajia.mvc.exceptions.CjjClientException
import com.alibaba.fastjson.JSON
import com.alibaba.fastjson.JSONObject
import org.apache.commons.collections.MapUtils
import org.apache.commons.lang3.StringUtils

/**
 * 百信客户信息更新接口请求参数
 *
 * @author liuchenglong @date 2021/07/13/11:42 上午
 */
class AiBankCustomerInfoUpdateRequest extends AibankCommonClientRequest {

    // 用户信息更新-交易码-百信金服给出
    private static final String TX_CD_UPDATE_CREDIT = "AICCSF0147"
    private static final String UPDATE_CREDIT_SOURCE_CODE = "BX10044"
    private static final String UPDATE_CREDIT_PRODUCT_CODE = "BX1004401"
    private static final String UPDATE_CREDIT_EVENT_TYPE = "updateCredit"

    // 用户信息处理常量
    private static final String CERT_TYPE = "101"
    private static final String MISSING_EXPIRATION_DATE = "21000101"
    private static final String LONG_TERM_VALIDITY = "99991231"
    private static final String LONG_TERM_VALIDITY_FLAG = "长期"


    @Override
    String buildRequest(GateWayMessageRequest req, MessageCommonContext context) throws Exception {
        if (Objects.isNull(req)) {
            return null
        }
        context.setInnerSystemRequest(req)
        String messageBody = req.getMessageBody()
        if (StringUtils.isBlank(messageBody)) {
            return null
        }
        commonLogger.info("百信客户信息更新接口请求处理 start, 请求messageBody:{}", messageBody)
        Map requestMap = JSONObject.parseObject(messageBody, Map.class)
        String creditApplyNo = requestMap.creditApplyNo
        // 构建明文body
        JSONObject aiBody = getBody(buildBusinessParams(requestMap))
        // 构建header
        String token = getToken(false, context, creditApplyNo)
        // 交易码-百信给，每个接口对应一个
        String tx_cd = getTxCd()
        JSONObject aiHeader = getAiBankHeader(context, aiBody, tx_cd, TO_SYSTEM_ID, MSG_TYPE, token)
        Map<String, JSONObject> requestParams = new HashMap<>(16)
        requestParams.put("head", aiHeader)
        requestParams.put("body", aiBody)

        commonLogger.info("百信客户信息更新接口请求处理 outerCreditOrderId:{}，客户信息更新报文:{}",
                creditApplyNo, JSONObject.toJSONString(requestParams))

        // body加密
        String encryptedResult = getEncryption(context, JSONObject.toJSONString(aiBody))
        // msg_content
        JSONObject msgContent = new JSONObject()
        msgContent.put("msg_content", encryptedResult)
        // 请求参数中body
        requestParams.put("body", msgContent)
        String requestParamsStr = JSONObject.toJSONString(requestParams)
        // 构建对方接口uri
        String uriStr = context.getMessageProtocolDto().getCommunication().getUri()
        String targetUri = buildUri(uriStr, APP_ID, TO_SYSTEM_ID, tx_cd)
        context.getMessageProtocolDto().getCommunication().setUri(targetUri)
        commonLogger.info("百信客户信息更新接口请求处理 加密后，creditApplyNo：{}，客户信息更新请求参数：{}，请求uri：{}",
                creditApplyNo, requestParamsStr, targetUri)

        return requestParamsStr
    }

    private static def buildBusinessParams(Map requestMap) {
        def customerInfoUpdateBo = [:]
        String creditApplyNo = requestMap.creditApplyNo
        customerInfoUpdateBo.outerCreditOrderId = creditApplyNo
        customerInfoUpdateBo.identityAuth = getIdentityAuth(requestMap)
        customerInfoUpdateBo.put("pull", getPullImages(creditApplyNo))
        def clientBasicInfo = getClientBasicInfo(requestMap)
        if (Objects.nonNull(clientBasicInfo)) {
            customerInfoUpdateBo.clientBasicInfo = clientBasicInfo
        }
        commonLogger.info("百信客户信息更新接口请求处理 businessParams:{}", JSON.toJSONString(customerInfoUpdateBo))
        return customerInfoUpdateBo
    }

    private static def getIdentityAuth(Map customerInfo) {
        JSONObject userInfo = customerInfo.userObtainInfo as JSONObject
        def identityAuth = [:]
        String name = customerInfo.name as String
        name = StringUtils.isBlank(name) ? (Objects.nonNull(userInfo) ? userInfo.name : null) : name
        String identificationNo = customerInfo.identificationNo
        String certNo = StringUtils.isBlank(identificationNo)
                ? (Objects.nonNull(userInfo) ? userInfo.identificationNo : null) : identificationNo
        String expiredDate = getExpiredDate(customerInfo)
        if (StringUtils.isBlank(name) || StringUtils.isBlank(certNo) || StringUtils.isBlank(expiredDate)) {
            throw new CjjClientException(CommonCodes.parameterError.code, String.format("百信客户信息更新接口请求处理 " +
                    "用户身份认证信息缺失, name:%s, certNo:%s, expiredDate:%s", name, certNo, expiredDate))
        }
        identityAuth.name = name
        identityAuth.certType = CERT_TYPE
        identityAuth.certNo = certNo
        identityAuth.expiredDate = expiredDate
        return identityAuth
    }

    private static String getExpiredDate(Map customerInfo) {
        String cardValidDateEnd = customerInfo.cardValidDateEnd as String
        String cardValidDate = customerInfo.cardValidDate as String
        // 先从传输字段中获取
        String expiredDate = getCardValidDateEnd(cardValidDateEnd, cardValidDate)
        // 身份证有效期结束时间取不到，再从用户取数信息中
        if (StringUtils.isBlank(expiredDate)) {
            JSONObject userInfo = customerInfo.userObtainInfo as JSONObject
            cardValidDateEnd = userInfo.cardValidDateEnd
            cardValidDate = userInfo.cardValidDate
            expiredDate = getCardValidDateEnd(cardValidDateEnd, cardValidDate)
        }
        if (StringUtils.isBlank(expiredDate)) {
            // 未记录有效期，取默认值
            return MISSING_EXPIRATION_DATE
        }
        return expiredDate
    }

    /**
     * 获取有效期结束时间 yyyyMMdd
     *
     * @param cardValidDateEnd 身份证有效期结束时间,格式yyyy.MM.dd 或长期
     * @param cardValidDate 证件有效期，格式：yyyy.MM.dd-yyyy.MM.dd or yyyy.MM.dd-长期
     * @return 有效期结束时间 yyyyMMdd
     */
    private static String getCardValidDateEnd(String cardValidDateEnd, String cardValidDate) {
        String expiredDate
        if (StringUtils.isNotBlank(cardValidDateEnd)) {
            expiredDate = cardValidDateEnd
            return Objects.equals(LONG_TERM_VALIDITY_FLAG, expiredDate)
                    ? LONG_TERM_VALIDITY : expiredDate.replace(".", "")
        }
        if (StringUtils.isNotBlank(cardValidDate)) {
            expiredDate = StringUtils.substring(cardValidDate, cardValidDate.lastIndexOf("-") + 1)
            return Objects.equals(LONG_TERM_VALIDITY_FLAG, expiredDate)
                    ? LONG_TERM_VALIDITY : expiredDate.replace(".", "")
        }
        return null
    }

    private static def getClientBasicInfo(Map customerInfo) {
        def clientBasicInfo = [:]
        String address = customerInfo.address
        if (StringUtils.isNotBlank(address)) {
            clientBasicInfo.address = address
        }
        String mobile = customerInfo.mobile
        if (StringUtils.isNotBlank(mobile)) {
            clientBasicInfo.phone = mobile
        }
        // 职业编码
        String occupationCode = transformOccupation(customerInfo.occupationCode as String)
        if (StringUtils.isNotBlank(occupationCode)) {
            clientBasicInfo.occupationType = occupationCode
        }
        String nationality = customerInfo.nationality
        if (StringUtils.isNotBlank(nationality)) {
            clientBasicInfo.nationality = nationality
        }
        if (MapUtils.isEmpty(clientBasicInfo)) {
            return null
        }
        return clientBasicInfo
    }

    /**
     * 资方职业编码
     * 0	国家机关、党群组织、企业、事业单位负责人
     * 1	专业技术人员
     * ３	办事人员和有关人员
     * ４	商业、服务业人员
     * ５	农、林、牧、渔、水利业生产人员
     * 6	生产、运输设备操作人员及有关人员
     * X	军人
     *
     * @param occupationCode 上游传输的职业编码
     * @return 百信所需职业编码
     */
    static String transformOccupation(String occupationCode) {
        if (StringUtils.isBlank(occupationCode)) {
            return ""
        }
        switch (occupationCode) {
            case "01": // 国家机关、党群组织、企业、事业单位负责人
                return "0"
            case "02": // 专业技术人员
                return "1"
            case "03": // 办事人员和有关人员
                return "3"
            case "04": // 商业人员
                return "4"
            case "05": // 农林牧渔水利业生产人员
                return "5"
            case "06": // 服务业人员
                return "4"
            case "07": // 军人
                return "X"
            default:
                return ""
        }

    }


    /**
     * 构建客户信息更新请求参数
     *
     * @return 请求 body
     */
    private static JSONObject getBody(Map reqMap) {
        JSONObject aiBody = new JSONObject()
        // 机构码
        aiBody.put("sourceCode", UPDATE_CREDIT_SOURCE_CODE)
        // 产品码
        aiBody.put("productCode", UPDATE_CREDIT_PRODUCT_CODE)
        // 交易码
        aiBody.put("eventType", UPDATE_CREDIT_EVENT_TYPE)
        // 外部业务订单号 UUID.randomUUID().toString()
        aiBody.put("outerOrderId", UUID.randomUUID().toString())
        // 业务参数
        aiBody.put("businessParams", reqMap)
        return aiBody
    }

    /**
     * 影像拉取列表
     *
     * @param creditApplyNo 授信申请流水号
     */
    private static List<JSONObject> getPullImages(String creditApplyNo) {
        String currentFilePre = String.join("/", FILE_PRE_PATH, creditApplyNo)

        List<JSONObject> images = new ArrayList<>()

        // 身份证正面
        JSONObject imageFront = new JSONObject()
        // 是否压缩文件 1压缩包 0非压缩包
        imageFront.put("isCompressed", "0")
        // 文件类型 IDA：身份证正面；IDB：身份证反面；facePhoto：人脸照
        imageFront.put("pullFileType", "IDA")
        // 拉取的文件名 文件路径/文件名
        String idFrontFileName = String.join("_", creditApplyNo, "ID_PORTRAIT.jpg")
        imageFront.put("pullFileName", idFrontFileName)
        // 拉取的文件地址 SFTP服务器地址
        String frontFileIndex = String.join("/", currentFilePre, "images", idFrontFileName)
        imageFront.put("pullFileIndex", frontFileIndex)
        images.add(imageFront)

        // 身份证反面
        JSONObject imageBack = new JSONObject()
        // 是否压缩文件 1压缩包 0非压缩包
        imageBack.put("isCompressed", "0")
        // 文件类型 IDA：身份证正面；IDB：身份证反面；facePhoto：人脸照
        imageBack.put("pullFileType", "IDB")
        // 拉取的文件名 文件路径/文件名
        String idBackFileName = String.join("_", creditApplyNo, "ID_EMBLEM.jpg")
        imageBack.put("pullFileName", idBackFileName)
        // 拉取的文件地址 SFTP服务器地址
        String backFileIndex = String.join("/", currentFilePre, "images", idBackFileName)
        imageBack.put("pullFileIndex", backFileIndex)
        images.add(imageBack)

        // 活体一张
        JSONObject facePhoto = new JSONObject()
        // 是否压缩文件 1压缩包 0非压缩包
        facePhoto.put("isCompressed", "0")
        // 文件类型 IDA：身份证正面；IDB：身份证反面；facePhoto：人脸照
        facePhoto.put("pullFileType", "facePhoto")
        // 拉取的文件名 文件路径/文件名
        String faceFileName = String.join("_", creditApplyNo, "LIVEFACE.jpg")
        facePhoto.put("pullFileName", faceFileName)
        // 拉取的文件地址 SFTP服务器地址
        String faceFileIndex = String.join("/", currentFilePre, "images", faceFileName)
        facePhoto.put("pullFileIndex", faceFileIndex)
        images.add(facePhoto)

        // 人行征信授权协议
        JSONObject rhzx = new JSONObject()
        // 是否压缩文件 1压缩包 0非压缩包
        rhzx.put("isCompressed", "0")
        // 文件类型 人行征信授权协议：credAuth
        rhzx.put("pullFileType", "credAuth")
        // 拉取的文件名 文件路径/文件名
        String rhzxFileName = String.join("_", creditApplyNo, "RHZXSQXY.pdf")
        rhzx.put("pullFileName", rhzxFileName)
        // 拉取的文件地址 SFTP服务器地址
        String rhzxFileIndex = String.join("/", currentFilePre, "credit", rhzxFileName)
        rhzx.put("pullFileIndex", rhzxFileIndex)
        images.add(rhzx)

        // 三方征信授权协议
        JSONObject sfzx = new JSONObject()
        // 是否压缩文件 1压缩包 0非压缩包
        sfzx.put("isCompressed", "0")
        // 文件类型 三方征信授权协议：perInfoAuth
        sfzx.put("pullFileType", "perInfoAuth")
        // 拉取的文件名 文件路径/文件名
        String sfzxFileName = String.join("_", creditApplyNo, "SFZXSQXY.pdf")
        sfzx.put("pullFileName", sfzxFileName)
        // 拉取的文件地址 SFTP服务器地址
        String sfzxFileIndex = String.join("/", currentFilePre, "credit", sfzxFileName)
        sfzx.put("pullFileIndex", sfzxFileIndex)
        images.add(sfzx)

        // 授信借款额度合同
        JSONObject jk = new JSONObject()
        // 是否压缩文件 1压缩包 0非压缩包
        jk.put("isCompressed", "0")
        // 文件类型 授信借款额度合同：creLoanCont
        jk.put("pullFileType", "creLoanCont")
        // 拉取的文件名 文件路径/文件名
        String jkFileName = String.join("_", creditApplyNo, "JKEDHT.pdf")
        jk.put("pullFileName", jkFileName)
        // 拉取的文件地址 SFTP服务器地址
        String jkFileIndex = String.join("/", currentFilePre, "credit", jkFileName)
        jk.put("pullFileIndex", jkFileIndex)
        images.add(jk)

        return images

    }

    private static String getTxCd() {
        return TX_CD_UPDATE_CREDIT
    }

}
