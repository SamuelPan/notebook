package cn.caijiajia.fundgwserver.groovy.aibank

import cn.caijiajia.fundgwserver.dto.MessageCommonContext
import cn.caijiajia.fundgwserver.handler.biz.AibankCommonClientResponse
import com.alibaba.fastjson.JSONObject
import org.apache.commons.lang3.StringUtils

/**
 * @author liuchenglong @date 2021/07/13/2:50 下午
 */
class AiBankCustomerInfoUpdateResponse extends AibankCommonClientResponse {

    @Override
    String buildResponse(String result, MessageCommonContext context) throws Exception {
        if (StringUtils.isBlank(result)) {
            return null
        }
        JSONObject quotaHandleResult = new JSONObject()
        JSONObject response = buildCommonResponse(result)
        // 通讯层
        if (Objects.equals(HEAD_PROCESS_CLIENT_SUCCESS, response.get(RESPONSE_RSPCODE))
                && StringUtils.isNotBlank((String) response.get(RESPONSE_ENCRYPTDATA))) {
            // ======= 交易成功 ========
            // 解密后的报文
            String decryptResponse = getDecrypt((String) response.get(RESPONSE_ENCRYPTDATA), context)

            logger.info("AiBankCustomerInfoUpdateResponse 客户信息更新返回报文：{}", decryptResponse)

            // 验签
            boolean verifySignResult = verifySign((String) response.get(RESPONSE_SIGN), context, decryptResponse)
            if (verifySignResult) {
                // ======= 验签成功 =======
                // 解密后的数据
                response.put(RESPONSE_DATA, decryptResponse)
                Map map = JSONObject.parseObject(decryptResponse, Map.class)
                // 外部业务订单号--对应hb放款时申请号
                String outerOrderId = (String) map.get("outerOrderId")
                // 业务状态
                String retStatus = (String) map.get("retStatus")
                // 业务状态信息
                String retMsg = (String) map.get("retMsg")
                quotaHandleResult.put("status", "SUCCESS")
                quotaHandleResult.put("result", outerOrderId)
                quotaHandleResult.put("retCode", retStatus)
                quotaHandleResult.put("retMsg", retMsg)
            } else {
                // ======= 验签失败 ========
                quotaHandleResult.put("status", "FAIL")
                quotaHandleResult.put("retCode", response.get(RESPONSE_RSPCODE))
                quotaHandleResult.put("retMsg", "签名验证失败")
            }
            return JSONObject.toJSONString(quotaHandleResult)
        } else if (Objects.equals(HEAD_PROCESS_TOKEN_FAIL, response.get(RESPONSE_RSPCODE))) {
            // ======== 获取token失败 ========
            // 重新获取token
            getToken(true, context, String.valueOf(System.currentTimeMillis()))
            quotaHandleResult.put("status", "PROCESS")
            quotaHandleResult.put("retCode", response.get(RESPONSE_RSPCODE))
            quotaHandleResult.put("retMsg", "获取token失败")
            return JSONObject.toJSONString(quotaHandleResult)
        }
        quotaHandleResult.put("status", "PROCESS")
        quotaHandleResult.put("retCode", response.get(RESPONSE_RSPCODE))
        quotaHandleResult.put("retMsg", response.get(RESPONSE_RETRESULT))
        return JSONObject.toJSONString(quotaHandleResult)
    }
}