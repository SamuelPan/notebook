package groovy.psbcxj.req

import cn.caijiajia.fundgwserver.dto.MessageCommonContext
import cn.caijiajia.fundgwserver.handler.AbstractReceiveRequestHandler
import com.alibaba.fastjson.JSON
import com.alibaba.fastjson.JSONObject
import groovy.json.JsonSlurper
import org.apache.commons.codec.binary.Base64
import org.json.JSONArray
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * 还款结果反向通知接口 请求报文处理
 */
class RepayReplyReceiveReq extends AbstractReceiveRequestHandler {


    private final static Logger logger = LoggerFactory.getLogger(RepayReplyReceiveReq.class)

    public final String REQ_TIME = "requestTime"

    public final String REQ_DATA = "requestData"

    public final String SYS_SIGN = "sysSign"


    /**
     * 构建请求参数
     *
     * @param req 外部系统参数
     * @param headerMap 外部系统的协议头部 参数
     * @param context {@link MessageCommonContext}
     * @return 请求参数，JSON字符串
     * @throws Exception
     */
    @Override
    String buildRequest(String req, Map<String, String> headerMap, MessageCommonContext context) throws Exception {
        logger.info("中邮消金【还款结果反向通知】解密前请求报文requestData:{}", JSONObject.parseObject(req))
        return receiveResponse(context, JSONObject.parseObject(req))
    }

    /**
     * 对资方的请求数据进行验签和解密处理，并重新组装参数
     *
     * @param context {@link MessageCommonContext}
     * @param request 请求参数
     * @return 组装完成后的参数
     */
    def receiveResponse(MessageCommonContext context, JSONObject request) {
        def transNotifyReqBo = new JSONObject()
        if (null != request.get(SYS_SIGN) && verify(context, request.getString(REQ_DATA), request.getString(SYS_SIGN))) {
            String decryData = decryData(context, request.getString(REQ_DATA))
            logger.info("中邮消金【还款结果反向通知】解密后请求报文：{}", decryData)
            def reqData = JSON.parseObject(decryData, Map.class)
            def result = [:]
            result.status = Objects.equals(reqData.tradeStatus, "1") ? "SUCCESS" : "FAIL"
            result.retMsg = reqData.failMsg ?: ""
            result.repayNo = reqData.outerRequestNo ?: ""
            result.loanNo = getLoanNo(reqData.repayDetailList as JSONArray)

            def fundcoreBizType = getFundcoreBizType(reqData.repaymentChannel)
            if (Objects.isNull(fundcoreBizType)) {
                logger.warn("中邮消金【还款结果反向通知】还款渠道为空或00,不做处理,原数据：{}", request)
                return null
            }
            transNotifyReqBo["bank"] = "PSBCXJ"
            transNotifyReqBo["notifyReq"] = JSONObject.toJSONString(result)
            transNotifyReqBo["fundcoreBizType"] = getFundcoreBizType(fundcoreBizType)
        } else {
            logger.warn("中邮消金【还款结果反向通知】验签失败，原数据：{}", request)
            return null
        }
        logger.info("中邮消金【还款结果反向通知】解密重新组装后请求报文：{}", transNotifyReqBo.toJSONString())
        return JSONObject.toJSONString(transNotifyReqBo)
    }

    /**
     * 获取借据号
     *
     * @param repayDetailList
     * @return 借据号
     */
    static def getLoanNo(JSONArray repayDetailList) {
        def repayDetail = repayDetailList.get(0) as JSONObject
        return repayDetail.subAcctNo
    }

    /**
     * 回调类型获取
     *
     * @param repaymentChannel 00:主动还款-主账户 01:批扣还款 02:催收还款 03:主动还款-子账户
     * @return 回调类型
     */
    static def getFundcoreBizType(def repaymentChannel) {
        // 还款回调
        if (Objects.equals("03", repaymentChannel)) {
            logger.info("中邮消金【还款结果反向通知】返回还款回调：{}", repaymentChannel)
            return "REPAY_NOTIFY"
        }
        // 批扣回调
        if (Objects.equals("01", repaymentChannel) || Objects.equals("02", repaymentChannel)) {
            logger.info("中邮消金【还款结果反向通知】返回批扣回调：{}", repaymentChannel)
            return "BATCH_DEDUCT_NOTIFY"
        }
        logger.info("中邮消金【还款结果反向通知】不继续执行回调：{}", repaymentChannel)
        return null
    }

    /**
     * 验签
     */
    boolean verify(MessageCommonContext context, String respData, String sign) {
        return encryptionService.verify(context, Base64.decodeBase64(respData), Base64.decodeBase64(sign))
    }

    /**
     * 解密
     */
    String decryData(MessageCommonContext context, String reqData) {
        return encryptionService.decryption(context, reqData)
    }

}
