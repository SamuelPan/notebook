package groovy.psbcxj.resp

import cn.caijiajia.fundgwserver.dto.MessageCommonContext
import cn.caijiajia.fundgwserver.handler.AbstractReceiveResponseHandler
import com.alibaba.fastjson.JSONObject
import groovy.json.JsonSlurper
import org.joda.time.DateTime
import org.slf4j.Logger
import org.slf4j.LoggerFactory


/*还款结果反向通知接口 响应报文处理*/
class RepayReplyReceiveResp extends AbstractReceiveResponseHandler{

    private final static Logger logger = LoggerFactory.getLogger(RepayReplyReceiveResp.class)

    public final String RESP_TIME = "responseTime"

    public final String CODE = "responseCode"

    public final String MSG = "responseMsg"

    public final String RESP_DATA = "responseData"

    public final String SYS_SIGN = "sysSign"

    public final String SUCCESS_CODE = "0000"


    @Override
    String buildResponse(String result, MessageCommonContext context) throws Exception {
        logger.info("中邮【还款结果反向通知】响应结果:{}", result)
        def data = new JsonSlurper().parseText(result) as Map
        def respData = [:]
        def msg
        respData.seqNo = UUID.randomUUID().toString()
        if (200 == data.code) {
            respData.agreeFlag = "SUC"
            msg = "处理成功"
        } else {
            respData.agreeFlag = "DEC"
            msg = "处理失败"
        }

        JSONObject responseData = new JSONObject()
        responseData.put(RESP_TIME, DateTime.now().toString("yyyyMMddHHmmss"))
        responseData.put(MSG, msg)
        responseData.put(CODE, SUCCESS_CODE)
        String encryData = encryData(context, JSONObject.toJSONString(respData))
        responseData.put(RESP_DATA, encryData)
        responseData.put(SYS_SIGN, signData(context, encryData))
        logger.info("中邮【还款结果反向通知】响应结果加密加签后:{}", responseData.toJSONString())
        return responseData.toJSONString()
    }

    /**
     * 加密
     */
    String encryData(MessageCommonContext context, String reqData) {
        return encryptionService.encryption(context, reqData)
    }

    /**
     * 加签
     */
    String signData(MessageCommonContext context, String encryData) {
        return encryptionService.sign(context, encryData)
    }
}
