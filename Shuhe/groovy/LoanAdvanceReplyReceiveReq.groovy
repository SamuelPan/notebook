package groovy.psbcxj.req

import cn.caijiajia.fundgwserver.dto.MessageCommonContext
import cn.caijiajia.fundgwserver.handler.AbstractReceiveRequestHandler
import com.alibaba.fastjson.JSONObject
import groovy.json.JsonSlurper
import org.apache.commons.codec.binary.Base64
import org.apache.commons.lang3.time.DateUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * 用信结果反向通知接口 请求报文处理
 */
class LoanAdvanceReplyReceiveReq extends AbstractReceiveRequestHandler {


    private final static Logger logger = LoggerFactory.getLogger(LoanAdvanceReplyReceiveReq.class)

    public final String REQ_TIME = "requestTime"

    public final String REQ_DATA = "requestData"

    public final String SYS_SIGN = "sysSign"


    /**
     * 构建请求参数
     *
     * @param req 外部系统参数
     * @param headerMap 外部系统的协议头部 参数
     * @param context {@link MessageCommonContext}
     * @return 请求参数，JSON字符串
     * @throws Exception
     */
    @Override
    String buildRequest(String req, Map<String, String> headerMap, MessageCommonContext context) throws Exception {
        logger.info("中邮消金【用信结果反向通知】解密前请求报文requestData:{}", JSONObject.parseObject(req))
        return receiveResponse(context, JSONObject.parseObject(req))
    }

    /**
     * 对资方的请求数据进行验签和解密处理，并重新组装参数
     *
     * @param context {@link MessageCommonContext}
     * @param request 请求参数
     * @return 组装完成后的参数
     */
    def receiveResponse(MessageCommonContext context, JSONObject request) {
        JSONObject transNotifyReqBo = new JSONObject()
        if (null != request.get(SYS_SIGN) && verify(context, request.getString(REQ_DATA), request.getString(SYS_SIGN))) {
            String decryData = decryData(context, request.getString(REQ_DATA))
            logger.info("中邮消金【用信结果反向通知】解密后请求报文：{}", decryData)
            def respJson = new JsonSlurper().parseText(decryData) as Map
            def result = [:]
            result.status = Objects.equals(respJson.loanStatus, "1") ? "SUCCESS" : "FAIL"
            result.loanNo = respJson.subAcctNo ?: ""
            result.actualLendDt = respJson.loanDate != null ? DateUtils.parseDate(respJson.loanDate as String, "yyyyMMdd") : new Date()
            result.retMsg = respJson.failMsg ?: ""
            result.loanRate = respJson.loanRate
            result.applyNo = respJson.outerRequestNo

            transNotifyReqBo["bank"] = "PSBCXJ"
            transNotifyReqBo["notifyReq"] = JSONObject.toJSONString(result)
            transNotifyReqBo["fundcoreBizType"] = "LEND_NOTIFY"
        } else {
            logger.warn("中邮消金【用信结果反向通知】验签失败，原数据：{}", request)
            return null
        }
        logger.info("中邮消金【用信结果反向通知】解密组装后请求报文：{}", transNotifyReqBo.toJSONString())
        return JSONObject.toJSONString(transNotifyReqBo)
    }

    /**
     * 验签
     */
    boolean verify(MessageCommonContext context, String respData, String sign) {
        return encryptionService.verify(context, Base64.decodeBase64(respData), Base64.decodeBase64(sign))
    }

    /**
     * 解密
     */
    String decryData(MessageCommonContext context, String reqData) {
        return encryptionService.decryption(context, reqData)
    }

}
