package groovy.psbcxj.req

import cn.caijiajia.fundgwserver.dto.MessageCommonContext
import cn.caijiajia.fundgwserver.handler.AbstractReceiveRequestHandler
import com.alibaba.fastjson.JSONObject
import groovy.json.JsonSlurper
import org.apache.commons.codec.binary.Base64
import org.apache.commons.lang3.time.DateUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/*授信结果反向通知接口 请求报文处理*/

class CreditApplyReplyReceiveReq extends AbstractReceiveRequestHandler {


    private final static Logger logger = LoggerFactory.getLogger(CreditApplyReplyReceiveReq.class)

    public final String REQ_TIME = "requestTime"

    public final String REQ_DATA = "requestData"

    public final String SYS_SIGN = "sysSign"


    /**
     * 构建请求参数
     *
     * @param req 外部系统参数
     * @param headerMap 外部系统的协议头部 参数
     * @param context {@link MessageCommonContext}
     * @return 请求参数，JSON字符串
     * @throws Exception
     */
    @Override
    String buildRequest(String req, Map<String, String> headerMap, MessageCommonContext context) throws Exception {
        logger.info("中邮消金【授信结果反向通知】解密前请求报文requestData:{}", JSONObject.parseObject(req))
        return receiveResponse(context, JSONObject.parseObject(req))
    }

    /**
     * 对资方的请求数据进行验签和解密处理，并重新组装参数
     *
     * @param context {@link MessageCommonContext}
     * @param request 请求参数
     * @return 组装完成后的参数
     */
    def receiveResponse(MessageCommonContext context, JSONObject request) {
        def transNotifyReqBo = new JSONObject()
        if (Objects.nonNull(request.get(SYS_SIGN)) && verify(context, request.getString(REQ_DATA), request.getString(SYS_SIGN))) {
            String decryData = decryData(context, request.getString(REQ_DATA))
            logger.info("中邮消金【授信结果反向通知】解密后请求报文：{}", decryData)
            def reqData = new JsonSlurper().parseText(decryData) as Map
            def result = [:]
            result.status = Objects.equals(reqData.approveStatus, "1") ? "SUCCESS" : "FAIL"
            result.retCode = reqData.failCode ?: ""
            result.retMsg = reqData.failMsg ?: ""
            result.reason = reqData.failMsg ?: ""
            result.limitAmt = reqData.containsKey("approveAmount") && reqData.approveAmount != null ? yuan2Fen(reqData.approveAmount) : 0
            result.applyNo = reqData.outerOrderNo
            result.creditNo = reqData.mainAcctNo
            result.creditEndDt = reqData.get("creditEndDate") != null ? DateUtils.parseDate(reqData.get("creditEndDate") as String, "yyyyMMdd") : null
            result.creditDate = reqData.get("creditStartDate") != null ? DateUtils.parseDate(reqData.get("creditStartDate") as String, "yyyyMMdd") : new Date()

            JSONObject respJson = new JSONObject()
            respJson.openId = reqData.openId
            respJson.rateType = reqData.rateType
            respJson.rateList = reqData.rateList
            result.respJson = respJson

            transNotifyReqBo["bank"] = "PSBCXJ"
            transNotifyReqBo["notifyReq"] = JSONObject.toJSONString(result)
            transNotifyReqBo["fundcoreBizType"] = "CREDIT_NOTIFY_BY_LEND"
        } else {
            logger.warn("中邮消金【授信结果反向通知】验签失败，原数据：{}", request)
            return null
        }
        logger.info("中邮消金【授信结果反向通知】解密组装后请求报文：{}", transNotifyReqBo.toJSONString())

        return transNotifyReqBo.toJSONString()
    }

    /**
     * 验签
     */
    boolean verify(MessageCommonContext context, String respData, String sign) {
        return encryptionService.verify(context, Base64.decodeBase64(respData), Base64.decodeBase64(sign))
    }

    /**
     * 解密
     */
    String decryData(MessageCommonContext context, String reqData) {
        return encryptionService.decryption(context, reqData)
    }

    static def yuan2Fen(money) {
        if (money == null) {
            return null
        }
        return new BigDecimal(money.toString()).movePointRight(2)
    }

}