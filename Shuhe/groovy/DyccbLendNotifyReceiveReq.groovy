package groovy.template.dyccb

import com.alibaba.fastjson.JSONObject

/**
 *
 * 东营银行放款回调请求参数处理
 *
 * @author liuchenglong @date 2021/08/12 上午10:12
 */
class DyccbLendNotifyReceiveReq extends DyccbCommonReceiveRequestHandler {

    private static final String LEND_NOTIFY = "LEND_NOTIFY"


    @Override
    String buildRequest(JSONObject requestBody) {
        log.info("东营银行【放款回调】外部系统请求参数:{}", requestBody)
        def requestParam = [:]
        requestParam.status = Objects.equals(requestBody.status, "1") ? "SUCCESS" : "FAIL"
        requestParam.retMsg = requestBody.statusMsg
        requestParam.applyNo = requestBody.merchantSerialNo
        requestParam.loanNo = requestBody.loanInvoiceId
        requestParam.seqNo = requestBody.seqNo
        def notifyReqBo = [:]
        notifyReqBo.bank = DYCCB
        notifyReqBo.notifyReq = requestParam
        notifyReqBo.fundcoreBizType = LEND_NOTIFY
        String notifyReqBoJSONString = JSONObject.toJSONString(notifyReqBo)
        log.info("东营银行【放款回调】组装完成的内部系统请求参数:{}", notifyReqBoJSONString)
        return notifyReqBoJSONString
    }
}
