package groovy.template.dyccb

import cn.caijiajia.fundgwcommon.code.CommonCodes
import cn.caijiajia.fundgwserver.dto.MessageCommonContext
import cn.caijiajia.fundgwserver.handler.AbstractReceiveRequestHandler
import cn.caijiajia.fundgwserver.util.Encodes
import cn.caijiajia.mvc.exceptions.CjjClientException
import com.alibaba.fastjson.JSON
import com.alibaba.fastjson.JSONObject
import groovy.util.logging.Slf4j
import org.apache.commons.collections.MapUtils
import org.apache.commons.lang3.StringUtils

import javax.crypto.Cipher
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec
import java.security.MessageDigest

/**
 *
 * 东营银行通用反向调用请求处理
 *
 * @author liuchenglong @date 2021/08/11 上午10:39
 */
@Slf4j
abstract class DyccbCommonReceiveRequestHandler extends AbstractReceiveRequestHandler {

    /**
     * 算法/模式/填充模式
     */
    protected static final String ALGORITHM_PATTERN_FILL_PATTERN = "AES/CBC/PKCS5Padding"

    protected static final String ENCRYPTED_KEY = "X-Uop-Encrypted-Key"

    protected static final String SIGNATURE = "X-Uop-Signature"

    protected static final String UNIQUE_NO = "X-Uop-Unique-No"

    protected static final String DYCCB = "DYCCB"

    protected static final String CHARSET_UTF_8 = "UTF-8"

    @Override
    String buildRequest(String req, Map<String, String> headerMap, MessageCommonContext context) throws Exception {
        def interfaceId = context.getMessageTemplateDto().getInterfacePo().getInterfaceId()
        log.info("东营银行反向调用请求,interfaceId:{},接口请求参数为:{},headerMap:{}",
                interfaceId, req, JSONObject.toJSONString(headerMap))
        checkNecessaryParam(req, headerMap, interfaceId)
        String data
        try {
            String sign = headerMap.get(SIGNATURE.toLowerCase())
            String encryptedKey = headerMap.get(ENCRYPTED_KEY.toLowerCase())
            String randomKey = encryptionService.decryption(context, Encodes.decodeBase64(encryptedKey))
            data = decryptByAES(req, randomKey)
            log.info("东营银行反向调用请求, AES 解密后,interfaceId:{}, data:{}", interfaceId, data)
            def signMap = new HashMap()
            def uniqueNo = headerMap.get(UNIQUE_NO.toLowerCase())
            signMap.put(UNIQUE_NO, uniqueNo)
            signMap.put("Body", SHA256(data))
            if (!encryptionService.verify(context,
                    compareToStr(signMap).getBytes(CHARSET_UTF_8),
                    Encodes.decodeBase64(sign))) {
                log.warn("东营银行反向调用请求,验签失败,interfaceId:{}, req:{}, MessageCommonContext:{}, sign:{}, signMap:{}",
                        interfaceId, req, JSON.toJSONString(context), sign, JSON.toJSONString(signMap))
            }
            log.info("东营银行反向调用请求,interfaceId:{},解密参数为:{}", interfaceId, data)
        } catch (Exception e) {
            log.warn("东营银行反向调用请求{}接口异常请求参数为:{}", interfaceId, req, e)
            throw new CjjClientException(CommonCodes.decryptionError.code, CommonCodes.decryptionError.message)
        }
        JSONObject jsonObject = JSONObject.parseObject(data)
        JSONObject uopData = jsonObject.uop_data as JSONObject
        if (Objects.isNull(uopData) || Objects.isNull(uopData.requestBody)) {
            log.info("东营银行反向调用请求,解密参数中requestBody缺失, interfaceId:{}, 解密参数{}", interfaceId, data)
            throw new CjjClientException(CommonCodes.parameterError.code, CommonCodes.parameterError.message)
        }
        JSONObject requestBody = uopData.requestBody as JSONObject
        JSONObject head = uopData.head as JSONObject
        requestBody.seqNo = head.seqNo
        requestBody.headMerchantSerialNo = head.merchantSerialNo
        return buildRequest(requestBody)
    }

    /**
     * 构建请求内部系统的参数
     *
     * @param reqJson 外部系统获取的请求参数
     * @return 内部系统构所需的参数
     */
    abstract String buildRequest(JSONObject requestBody)

    static void checkNecessaryParam(String req, Map<String, String> headerMap, String interfaceId) {
        if (MapUtils.isEmpty(headerMap)) {
            log.warn("东营银行反向调用请求,header信息存在缺失，interfaceId:{}, req:{} ,headerMap:{}",
                    interfaceId, req, JSON.toJSONString(headerMap))
            throw new CjjClientException(CommonCodes.parameterError.code, CommonCodes.parameterError.message)
        }
        if (Objects.isNull(req)) {
            log.warn("东营银行反向调用请求,接口请求参数存在缺失,interfaceId:{},req:{}", interfaceId, req)
            throw new CjjClientException(CommonCodes.parameterError.code, CommonCodes.parameterError.message)
        }
        def sign = headerMap.get(SIGNATURE.toLowerCase())
        def encryptedKey = headerMap.get(ENCRYPTED_KEY.toLowerCase())
        if (StringUtils.isBlank(sign) || StringUtils.isBlank(encryptedKey)) {
            log.warn("东营银行反向调用请求,密文签名信息缺失,interfaceId:{},req:{},sign:{},encryptedKey:{}",
                    interfaceId, req, sign, encryptedKey)
            throw new CjjClientException(CommonCodes.parameterError.code, CommonCodes.parameterError.message)
        }
    }

    /**
     * AES 解密
     *
     * @param content 待解密内容
     * @param key 加密时使用的盐
     * @return 解密结果
     */
    static String decryptByAES(String content, String key) {
        log.info("东营银行反向调用请求, decryptByAES 方法, content:{}, key:{}", content, key)
        try {
            Cipher cipher = Cipher.getInstance(ALGORITHM_PATTERN_FILL_PATTERN)
            IvParameterSpec iv = new IvParameterSpec(initIv(ALGORITHM_PATTERN_FILL_PATTERN))
            cipher.init(2, new SecretKeySpec(Base64.getDecoder().decode(key.getBytes()), "AES"), iv)
            byte[] cleanBytes = cipher.doFinal(Base64.getDecoder().decode(content.getBytes()))
            return new String(cleanBytes, CHARSET_UTF_8)
        } catch (Exception var6) {
            log.warn("AES解密失败, Aes content:{}", content, var6)
            return null
        }
    }

    /***
     * 初始化IV
     * @param fullAlg
     * @return
     */
    private static byte[] initIv(String fullAlg) {
        byte[] iv
        int i
        try {
            Cipher cipher = Cipher.getInstance(fullAlg)
            int blockSize = cipher.getBlockSize()
            iv = new byte[blockSize]

            for (i = 0; i < blockSize; ++i) {
                iv[i] = 0
            }

            return iv
        } catch (Exception var5) {
            log.warn("initIv exist exception:{}", var5)
            int blockSize = 16
            iv = new byte[blockSize]

            for (i = 0; i < blockSize; ++i) {
                iv[i] = 0
            }

            return iv
        }
    }

    static String SHA256(final String strText) {
        try {
            MessageDigest messageDigestIn = MessageDigest.getInstance("SHA-256")
            messageDigestIn.update(strText.getBytes(CHARSET_UTF_8))
            byte[] str = messageDigestIn.digest()
            if (str != null) {
                return Base64.getEncoder().encodeToString(str)
            }
        } catch (Exception e) {
            log.warn("SHA-256异常", e)
            return null
        }
        return null
    }

    /***
     * 排序获取Str
     *
     * @param signMap
     * @return 排序获取的Str
     */
    static String compareToStr(def signMap) {
        StringBuilder content = new StringBuilder()
        List<String> keys = new ArrayList(signMap.keySet())
        Collections.sort(keys)
        for (int i = 0; i < keys.size(); ++i) {
            String key = (String) keys.get(i)
            String value = (String) signMap.get(key)
            content.append(i == 0 ? "" : "&").append(key).append("=").append(value)
        }
        return content.toString()
    }

    static def yuan2Fen(money) {
        if (money == null) {
            return null
        }
        return new BigDecimal(money.toString()).movePointRight(2)
    }
}