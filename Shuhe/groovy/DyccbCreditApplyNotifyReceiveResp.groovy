package groovy.template.dyccb


import com.alibaba.fastjson.JSONObject

/**
 *
 * 东营银行授信回调响应参数处理
 *
 * @author liuchenglong @date 2021/08/12 下午6:02
 */
class DyccbCreditApplyNotifyReceiveResp extends DyccbCommonReceiveResponseHandler {

    @Override
    def buildBody(JSONObject param) {
        log.info("东营银行【授信回调】内部系统响应参数:{}", param)
        def body = [:]
        if (Objects.equals(param.code, SUCCESS_CODE)) {
            body.code = "1"
            body.statusMsg = "处理成功"
        } else {
            body.code = "0"
            body.statusMsg = "处理失败"
        }
        log.info("东营银行【授信回调】body:{}", param)
        return body
    }
}
