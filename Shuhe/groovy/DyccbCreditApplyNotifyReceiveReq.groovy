package groovy.template.dyccb

import com.alibaba.fastjson.JSONObject

/**
 *
 * 东营银行授信回调请求参数处理
 *
 * @author liuchenglong* @date 2021/08/11 下午4:57
 */
class DyccbCreditApplyNotifyReceiveReq extends DyccbCommonReceiveRequestHandler {

    private static final String CREDIT_NOTIFY_BY_LEND = "CREDIT_NOTIFY_BY_LEND"

    @Override
    String buildRequest(JSONObject requestBody) {
        log.info("东营银行【授信回调】外部系统请求参数:{}", requestBody)
        def requestParam = [:]
        requestParam.status = Objects.equals(requestBody.status, "1") ? "SUCCESS" : "FAIL"
        requestParam.retMsg = requestBody.statusMsg
        requestParam.applyNo = requestBody.merchantSerialNo ?: requestBody.headMerchantSerialNo
        requestParam.limitAmt = yuan2Fen(requestBody.useAmt ?: 0)
        requestParam.creditEndDt = requestBody.creditStartDate
        requestParam.creditDate = requestBody.creditEndDate
        requestParam.seqNo = requestBody.seqNo
        def notifyReqBo = [:]
        notifyReqBo.bank = DYCCB
        notifyReqBo.notifyReq = requestParam
        notifyReqBo.fundcoreBizType = CREDIT_NOTIFY_BY_LEND
        String notifyReqBoJSONString = JSONObject.toJSONString(notifyReqBo)
        log.info("东营银行【授信回调】组装完成的内部系统请求参数:{}", notifyReqBoJSONString)
        return notifyReqBoJSONString
    }

}
