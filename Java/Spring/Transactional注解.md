## Transactional



#### 一、源码

```java
package org.springframework.transaction.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.springframework.core.annotation.AliasFor;

@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface Transactional {
  
  	// 可选的限定描述符,指定使用的事务管理器
    @AliasFor("transactionManager")
    String value() default "";

    @AliasFor("value")
    String transactionManager() default "";

    // 可选的事务传播行为设置
    Propagation propagation() default Propagation.REQUIRED;

    // 可选的事务隔离级别设置
    Isolation isolation() default Isolation.DEFAULT;

    // 事务超时时间设置
    int timeout() default -1;

    // 读写或只读事务,默认读写
    boolean readOnly() default false;

    // 导致事务回滚的异常类数组
    Class<? extends Throwable>[] rollbackFor() default {};

    // 导致事务回滚的异常类名字数组
    String[] rollbackForClassName() default {};

    // 不会导致事务回滚的异常类数组
    Class<? extends Throwable>[] noRollbackFor() default {};

    // 不会导致事务回滚的异常类名字数组
    String[] noRollbackForClassName() default {};
}
```



----



#### 二、相关属性

**propagation各属性值含义:**

- Propagation.REQUIRED:如果有事务,那么加入事务,没有的话新创建一个
- Propagation.NOT_SUPPORTED:这个方法不开启事务
- Propagation.REQUIREDS_NEW:不管是否存在事务,都创建一个新的事务,原来的挂起,新的执行完毕,继续执行老的事务
- Propagation.MANDATORY:必须在一个已有的事务中执行,否则抛出异常
- Propagation.NEVER:不能在一个事务中执行,就是当前必须没有事务,否则抛出异常
- Propagation.SUPPORTS:若当前存在事务,则在事务中运行.否则以非事务形式运行
- Propagation.NESTED:如果当前存在事务,则运行在一个嵌套的事务中,如果当前没有事务,则按照REQUIRED属性执行.它只对DataSourceTransactionManager事务管理器起效

关于Spring如何处理该属性参见AbstractPlatformTransactionManager类的getTransaction方法.



**isolation各属性值含义:**

1. TransactionDefinition.ISOLATION_DEFAULT:这是默认值,表示使用底层数据库的默认隔离级别.
2. TransactionDefinition.ISOLATION_READ_UNCOMMITTED:该隔离级别表示一个事务可以读取另一个事务修改但还没有提交的数据.该级别不能防止脏读,不可重复读和幻读,因此很少使用该隔离级别.比如PostreSQL实际上并没有此级别.
3. TransactionDefinition.ISOLATION_READ_COMMITTED:该隔离级别表示一个事务只能读取另一个事务已经提交的数据.该级别可以防止脏读,这也是大多数情况下的推荐值.
4. TransactionDefinition.ISOLATION_REPEATABLE_READ:该隔离级别表示一个事务在整个过程中可以多次重复执行某个查询,并且每次返回的记录都相同.该级别可以防止脏读和不可重复读.
5. TransactionDefinition.ISOLATION_SERIALIZABLE:所有的事务依次逐个执行,这样事务之间就完全不可能产生干扰,也就是说,该级别可以防止脏读、不可重复读以及幻读.但是这将严重影响程序的性能,通常情况下也不会用到该级别.